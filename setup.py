#!/usr/bin/env python
# mongiris setup.py (uses setup.cfg)

# pdoc mongiris/api.py -o doc
# pandoc paper.md --filter pandoc-citeproc -o paper.pdf
# python3 -m setup bdist_wheel sdist
# python3 -m pip install -e mongiris/
# python3 -m pip install git+https://fduchate@gitlab.liris.cnrs.fr/fduchate/mongiris.git#egg=mongiris

from setuptools import setup
import shutil


def delete_dir(directories):  # delete directories (ignore errors such as read-only files)
    for directory in directories:
        shutil.rmtree(directory, ignore_errors=True)


# delete build/config directories because egg-info only updates config files for new versions
delete_dir(['./mongiris.egg-info/', 'pip-wheel-metadata', './build/', './dist/'])

readme = open("README.md").read()

setup(long_description=readme) # + "\n\n" + history)

print("""Mongiris has been installed.

Do not forget to load the 'dbinsee' database into MongoDB:

\t\tDownload the dump from https://gitlab.liris.cnrs.fr/fduchate/mongiris/raw/master/mongiris/data/dump/dump-dbinsee.bin

\t\tRun in a terminal: mongorestore --archive=/path/to/dump-dbinsee.bin
""")

