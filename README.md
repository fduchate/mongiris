# mongiris package

**Note: a web application (predihood) is available at [https://gitlab.com/fduchate/predihood](https://gitlab.com/fduchate/predihood) for visualizing (and predicting) neighbourhoods data.**

This Python package is an interface for querying neighbourhoods stored as documents in MongoDB.

The package mongiris includes a dataset of 50,000 French neighbourhoods (`hil`) used in a [research project](https://imu.universite-lyon.fr/appels-en-cours-et-bilans/2017-en-cours/hil-artificial-intelligence-to-facilitate-property-searches-system-of-recommendations-with-spatial-and-non-spatial-visualisation-for-property-search-2017/) to predict the environment of a neighbourhood (e.g., social class, type of landscape) based on hundreds of indicators (about population, shops, buildings, etc.). In this context, neighbourhoods are perceived as [IRIS](https://www.insee.fr/fr/metadonnees/definition/c1523) (a French administrative unit area).

The tool also includes a small test dataset (`bird-migration`) to demonstrate how to add new datasets. 

## Installation

### Installation using Docker

Check [https://gitlab.com/fduchate/predihood](https://gitlab.com/fduchate/predihood) for the Docker installation.

### Manual installation

Prerequisites :

- Python, version >=3
- [MongoDB](https://www.mongodb.com/), version >=4, in which it is necessary to import the datasets.

First, clone the repository with the following command:

```
git clone https://gitlab.liris.cnrs.fr/fduchate/mongiris
```

To install mongiris (and its dependencies), go in the `mongiris` directory and run:

```
python3 -m pip install -e .
```

Then import datasets into the MongoDB database: run the MongoDB server (`mongod`) and execute the following commands (from the MongoDB's executable directory if needed):

```
# import dataset 'hil' as a MongoDB dump
./mongorestore --archive=/path/to/dump-dbinsee.bin
# import dataset 'bird-migration' as a collection of JSON documents
./mongoimport --db=dbmigration -c=collmigration --file=/path/to/dump-bird-neighbourhoods.json	
./mongoimport --db=dbmigration -c=collindic --file=/path/to/dump-bird-indicators.json
```

where `/path/to/` is the path to the dataset files (provided with the package mongiris in `mongiris/data/dumps/`). A tip is to move the dataset files into the MongoDB binary (`PATH/TO/MONGODB/bin`). You may have to create these folders for Mongodb: `data/db` under `PATH/TO/MONGODB/bin` and run `./mongod --dbpath=./data/db`. 
This restoration may take a few minutes as the geospatial indexes are rebuilt for dataset _hil_.

## Datasets

More details about the datasets are provided at [https://gitlab.com/fduchate/predihood](https://gitlab.com/fduchate/predihood).

## Usage

To manipulate the database, simply connect to MongoDB (default on '127.0.0.1:27017') by creating an object of the `Mongiris` class.
Using this object, around 20 methods are available for querying the data.

Below is a minimal example of connection and queries (from `mongiris/tests/dummy.py` file):

```
from mongiris.api import Mongiris

db = Mongiris()

# return the number of documents in a collection
counts = db.count_documents(db.collection_indicators, {})

# get complete information about iris identified with code 593500203
iris = db.find_one_document(db.collection_neighbourhoods, {"properties.CODE_IRIS": "593500203"})
print(iris)

# get iris which contains coordinates 3.685111, 46.514643
iris2 = db.point_in_which_neighbourhood([3.685111, 46.514643])
print(iris2)
```

More examples, including testing geospatial queries, are available in the `tests/api_tests.py` file.


## Contributors

- Fabien Duchateau, Franck Favetta, Nelly Barret (laboratory [LIRIS](https://liris.cnrs.fr/), Université Lyon 1)

- Loïc Bonneval (laboratory [CMW](https://www.centre-max-weber.fr/), Université Lyon 2)


## Acknowledgments

Data source provider : 
- [INSEE](https://www.insee.fr/)
- [IGN](http://professionnels.ign.fr/contoursiris)

Financial support : 
- Labex [Intelligence des Mondes Urbains (IMU)](http://imu.universite-lyon.fr/projet/hil)

