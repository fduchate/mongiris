# import-data.sh: restores Mongo data if needed
# mongod should be already launched by the container

echo "Executing import-data.sh"

dbs=$(mongo --eval 'db.getMongo().getDBNames()')
echo "$dbs";

# check whether database dbinsee (dataset hil) exists, and imports data if needed 
if [ $(mongo --eval 'db.getMongo().getDBNames().indexOf("dbinsee")' --quiet) -lt 0 ]; then
    echo "Database dbinsee does not exist (dataset 'hil'), restoring dbinsee."
	mongorestore --archive=/tmp/dumps/dump-dbinsee.bin;
else
    echo "Database dbinsee already exists (dataset 'hil'), not restoring data."
fi

# check whether database dbmigration exists, and imports data if needed 
if [ $(mongo --eval 'db.getMongo().getDBNames().indexOf("dbmigration")' --quiet) -lt 0 ]; then
    echo "Database dbmigration does not exist (dataset 'bird-migration'), importing collections in dbmigration."
	mongoimport --db=dbmigration -c=collmigration --file=/tmp/dumps/dump-bird-neighbourhoods.json	
	mongoimport --db=dbmigration -c=collindic --file=/tmp/dumps/dump-bird-indicators.json
else
    echo "Database dbmigration already exists (dataset 'bird-migration'), not restoring data."
fi

echo "End of script import-data.sh"

