#!/usr/bin/env python
# encoding: utf-8
# =============================================================================
#   XLS utilities, mainly for INSEE indicators (parsing Excel files, etc.) and mobilipass data from HiL
#   https://pypi.python.org/pypi/openpyxl/
#   https://xlrd.readthedocs.io/en/latest/api.html
# =============================================================================

import sys
from mongiris import config
import xlrd # for old Excel files (.xls) and new files (.xlsx Excel 2010)
#import openpyxl  # for new Excel files (.xlsx Excel 2010)


def parse_xls_to_dict(xls_file_path):
    """
    Parse an XLS file (excel/calc) produced by INSEE and containing indicators about IRIS.
    :param xls_file_path: the path to the XLS file to be parsed
    :return: indicator_metadata: a list of dict containing information about indicator labels (shortname, fullname)
    :return: indicators: a dictionary such as {id_iris1: {ind1: val1, ind2: val2, ...}, id_iris2: {ind1: val1, ...}, ...}
    :return: source_metadata: a dictionary containing metadata information about the document (title, filepath, etc.)
    """
    indicators = dict()
    source_metadata = dict()  # metadata about the document (name, date mise en ligne, infoGeo, etc.)
    indicator_metadata = list()  # list of dict, each containing metadata about an indicator (short and full labels)
    try:
        wb = xlrd.open_workbook(xls_file_path,
                                ragged_rows=True)  # ragged_rows to True to avoid empty cells at the end of rows)
    except Exception as e:
        sys.exit(f'Error while parsing xlsx file {xls_file_path}: {e}')
    # sheet = wb.sheet_by_name("IRIS")  # data is stored in the sheet "IRIS", else wb.sheet_names()
    sheet = wb.sheet_by_index(0)  # sheet are sometimes called IRIS, also IRIS_DEC

    # extracting  source metadata
    source_metadata['filepath'] = xls_file_path  # filepath of the document
    source_metadata['title'] = sheet.cell_value(0, 0)  # title of the document
    source_metadata['infoGeo'] = sheet.cell_value(1, 0)  # geographic information (area + level of granularity)
    cell_dates = sheet.cell_value(2, 0)
    if cell_dates.startswith("Mise en ligne le "):
        cell_dates = cell_dates[17:27]
    source_metadata['datePublication'] = cell_dates  # date of online publication

    # extracting labels/fieldnames
    long_fieldnames = sheet.row_values(4)  # row 4 contains the long labels
    short_fieldnames = sheet.row_values(5)  # row 5 contains the short labels (usually not meaningful)
    for i in range(0, len(short_fieldnames)):
        shortname = short_fieldnames[i]
        longname = long_fieldnames[i]
        ind_dict = {config.geojson_shortname_label: shortname, config.geojson_longname_label: longname,
                    config.geojson_from_files_label: [xls_file_path]}
        indicator_metadata.append(ind_dict)

    # extracting indicators values
    nb_fields = len(short_fieldnames)
    for i in range(6, sheet.nrows, 1):
        iris_id = sheet.cell_value(i, 0)  # IRIS id is in the first column
        # print(sheet.row_values(i))
        if sheet.row_len(i) == nb_fields:  # some rows may not include all fields
            if iris_id not in indicators:
                indicators[iris_id] = dict()
            for j in range (0, nb_fields):
                field = short_fieldnames[j]  # + "  -  " + long_fieldnames[j]
                val = sheet.cell_value(i, j)
                indicators[iris_id][field] = val
        else:
            print("\tIgnored row (missing fields) : " + str(sheet.row_values(i)))
    return indicator_metadata, indicators, source_metadata


def parse_data_mobilipass_to_dict(xls_file_path):
    data = dict()
    try:
        wb = xlrd.open_workbook(xls_file_path,
                                ragged_rows=True)  # ragged_rows to True to avoid empty cells at the end of rows)
    except Exception as e:
        sys.exit('Error while parsing XLS file {}: {}'.format(xls_file_path, e))
    sheet = wb.sheet_by_name("Mobilipass - 6") # ("Mobilipass")
    long_fieldnames = sheet.row_values(1)  # row 2 contains the long labels
    nb_fields = len(long_fieldnames)
    for i in range(2, sheet.nrows, 1):
        person_id = sheet.cell_value(i, 0)  # person id is in the first column
        if sheet.row_len(i) == nb_fields:  # some rows may not include all fields
            if person_id not in data:
                data[person_id] = dict()
            for j in range (0, nb_fields):
                field = long_fieldnames[j]
                val = sheet.cell_value(i, j)
                data[person_id][field] = val
        else:
            print("Ignored row: " + str(sheet.row_values(i)))
    return long_fieldnames, data

