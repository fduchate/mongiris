#!/usr/bin/env python
# encoding: utf-8
# =============================================================================
# Integrator: perform the integration of different data sources to produce MongoDB documents (GeoJSON format) containing
# IRIS information (~640 or ~350 INSEE indicators, depending on the IRIS).
# This file does not need to be run because the MongoDB dump is provided (unless for integrating new data sources).
# =============================================================================
# Path to MongoDB tools (under MacOS): /Applications/MongoDB.app/Contents/Resources/Vendor/mongodb/bin/
# Export and import a local database (metadata/indexes included, option dryRun for testing, several files):
# mongodump --db dbinsee --verbose -o dump/
# mongorestore --db dbinsee --dir dump/dbinsee --verbose --dryRun
# Export and import a local database (metadata/indexes included, option dryRun for testing, single binary file):
# mongodump --db dbinsee --verbose --archive=dbinsee.bin
# mongorestore --archive=dbinsee.bin --verbose
# Export and import in JSON:
# ./mongoexport --db dbinsee --out ~/dump-iris.json
# ./mongoimport --db dbinsee --file ~/dump-iris.json
# =============================================================================

import os
from os import path
import logging
from mongiris import xls_utils
from mongiris import api

collection_indic = "collindic"
collection_sources = "collsources"
# labels for geojson iris files and for the json indicators dictionary
geojson_raw_indicators_label = 'raw_indicators'
geojson_grouped_indicators_label = 'grouped_indicators'
geojson_insee_files_label = 'insee_files'
geojson_shortname_label = 'short_label'
geojson_longname_label = 'full_label'
geojson_from_files_label = "from_insee_files"
# data parameters
insee_dir = path.join('data', 'insee', '2019-01')  # the date indicates the last check for updates of INSEE files
# the following labels are metadata about an IRIS, not indicators
labels_dictionary_init = {'CODE_IRIS': {"long_fieldname": 'Code IRIS'}, 'NOM_IRIS': {"long_fieldname": 'Nom IRIS'},
                          'INSEE_COM': {"long_fieldname": 'Code postal commune'},
                          'NOM_COM': {"long_fieldname": 'Nom commune'}, 'IRIS': {"long_fieldname": 'Code IRIS'},
                          'TYP_IRIS': {"long_fieldname": 'Type IRIS'}, 'REG': {"long_fieldname": 'Code région'},
                          'DEP': {"long_fieldname": 'Code département'}, 'TRIRIS': {"long_fieldname": 'TRIRIS'},
                          'UU2010': {"long_fieldname": 'Unité urbaine'}, 'LIBIRIS': {"long_fieldname": 'Libellé IRIS'},
                          'GRD_QUART': {"long_fieldname": 'Grand quartier'},
                          'LAB_IRIS': {"long_fieldname": 'Label qualité IRIS'},
                          'MODIF_IRIS': {"long_fieldname": 'Type de modification de IRIS'},
                          }


def get_all_xlsx_files(dir_sources):
    """
    Returns a list of spreadsheets files to be integrated.

    Args:
        dir_sources: path to a directory containing data sources (spreadsheets from INSEE)

    Returns:
        insee_files: a list of filepaths (to xls files)

    """
    insee_files = list()
    for file in os.listdir(dir_sources):
        filepath = os.path.join(insee_dir, file)
        if os.path.isfile(filepath) and filepath.endswith(".xls"):
            insee_files.append(filepath)
    return insee_files


def integrate_xls_file(connection, xls_file):
    """
    Integrates data from an xsl file : the 3 collections may be updated. First, extracts information about the source
    (e.g., title, date) and inserts a document for that source in 'collsources'. Next extracts information about the
    indicators (e.g., short name, long name) and updates/inserts a document in 'collindic'. Finally for each line (IRIS)
    in the spreadsheet, updates corresponding document with new indicators.

    Args:
        connection: an object representing the database connection
        xls_file: a csv file path containing INSEE indicators
    """
    indicator_metadata, indicators, source_metadata = xls_utils.parse_xls_to_dict(xls_file)

    # add the source metadata into collection_sources
    doc = connection.find_one_document(collection_sources, {"filename": xls_file})
    if doc is None:
        connection.insert_one_document(collection_sources, source_metadata)

    # add the indicators information into collection_indic
    for ind in indicator_metadata:
        short_name = ind[geojson_shortname_label]
        doc = connection.find_one_document(collection_indic, {geojson_shortname_label: short_name})
        if doc is not None:  # only update field from_insee_files, $addToSet does not add duplicate values
            connection.update_one_document(collection_indic, {geojson_shortname_label: short_name},
                                           {"$addToSet": {geojson_from_files_label: xls_file}})
        else:  # add the document
            connection.insert_one_document(collection_indic, ind)

    # add the indicators values into collection_iris
    not_found_iris = found_iris = nb_replacements = 0
    for code_iris, indics in indicators.items():
        doc = connection.get_neighbourhood_from_code(code_iris)
        if doc is None:
            not_found_iris += 1
        else:
            doc_id = doc['properties']['CODE_IRIS']
            found_iris += 1
            need_replacement = False
            doc_changes = {"$set": dict()}  # an update document containing the new fields
            for ind, value in indics.items():
                if ind in labels_dictionary_init:  # main indicator, do not replace it if already there
                    if ind not in doc['properties']:  # general information (code iris, city name, etc.)
                        doc_changes["$set"]["properties." + ind] = value
                        need_replacement = True
                else:  # raw indicator
                    if ind not in doc['properties'][geojson_raw_indicators_label]:  # add this value
                        doc_changes["$set"]["properties." + geojson_raw_indicators_label + "." + ind] = value
                        need_replacement = True

            if need_replacement:  # replace the old doc by new doc
                res = connection.update_one_document(connection.collection_neighbourhoods,
                                                     {'properties.CODE_IRIS': doc_id}, doc_changes)
                nb_replacements += res.modified_count

    logger.info(f"\t\t{xls_file}: {found_iris} found iris, {not_found_iris} not found iris, {nb_replacements} updates")
    return True


def integrate(connection, dir_sources):
    """
    Main integration method: parses a directory of data sources, and call the `integrate_xls_file` function for each
    data source.

    Args:
        connection: an object representing the database connection
        dir_sources: path to a directory containing data sources (spreadsheets from INSEE)
    """
    logger.info("Searching xlsx files...")
    insee_files = get_all_xlsx_files(dir_sources)  # get the list of all xlsx files to be integrated
    logger.info(f"Found a total of {len(insee_files)} xlsx files to be integrated.")
    #  insee_files = [] # roughly 10-20 mins execution time for one data source
    logger.info(f'Selected {len(insee_files)} xlsx files to be integrated.')

    logger.info("Integrating sources files (metadata for source and indicators, data for iris)")
    for file in insee_files:  # integrate each xlsx INSEE file (both indicators in IRIS and indicators dict)
        logger.info(f"\t- INSEE xlsx file: {file}")
        integrate_xls_file(connection, file)
    logger.info("Done !")


def check_properties(connection):
    """
    Computes and prints statistics on the IRIS collection, mainly the number of indicators per document.
    Args:
        connection: an object representing the database connection
    """
    docs = connection.find_documents(connection.collection_neighbourhoods, {}, )
    counts = dict()
    for doc in docs:
        nb = len(doc['properties'][geojson_raw_indicators_label])
        if nb not in counts:
            counts[nb] = 0
        counts[nb] += 1
    print(counts)


#########################
# main integration script
#########################

if __name__ == '__main__':
    logging.basicConfig(format='[%(levelname)s] - %(name)s - %(asctime)s : %(message)s')
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    connection_hil = api.Mongiris("dbinsee", "colliris")  # database connection
    integrate(connection_hil, insee_dir)  # integrating data sources
    check_properties(connection_hil)  # stats about properties: 5min exec., {350: 36530, 638: 11738, 615: 1057, 373: 79}
