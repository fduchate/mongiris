#!/usr/bin/env python
# encoding: utf-8
# =============================================================================
# Abstraction layer for the MongoDB database
# Performs operations such as find, update, convert_geojson_files, intersect, etc.
# Some methods are not static because they require a valid DB connection (performed in __init__)
# =============================================================================

import pymongo
from bson import json_util  # used to convert BSON to JSON (especially ObjectId type of "_id")
import json
import logging

max_timeout = 300000  # delay before connection timeout in milliseconds

class Mongiris:
    """
    The Mongiris class is an API to manipulate data from a MongoDB database containing neighbourhoods (GeoJSON format).

    Several methods accepts a 'collection' parameter for flexibility (i.e., be able to query other collections than the
    neighbourhoods collection).
    Most methods convert resulting documents from the BSON format (MongoDB) to JSON. This mainly avoids the issue of
    ObjectId type (for MongoDB '_id' field).

    The constructor initializes the logger and it automatically connects to the database.
    The name of the database and of the three collections
    are based on the default names (the ones from the dump). If the names are changed in MongoDB, they should be changed
    in the `config.py` file.

    Examples of usages, including testing geospatial queries, are available in `tests/api_tests.py`.

    An example of neighbourhood following the GeoJSON format is provided in `data/example-neighbourhood.json`.

    Args:
        host: the server hosting MongoDB (string, default to 'localhost')
        port: the port number on which MongoDB listens to (integer, default tp 27017)
        db: the MongoDB database name to connect to (string)
        coll_neighbourhoods: the name of the neighbourhoods collection, inside database db (string)
        coll_indic: the name of the indicators collection, inside database db (string)

    Additional resources:

    - [MongoDB documentation](http://www.mongodb.org/)

    - [GeoJSON format specifications](https://geojson.org/)

    - [pymongo API](http://api.mongodb.com/python/current/) between Python and MongoDB

    """

    def __init__(self, host='localhost', port=27017, db='dbinsee', coll_neighbourhoods='colliris', coll_indic='collindic'):
        logging.basicConfig(format='[%(levelname)s] - %(name)s - %(asctime)s : %(message)s')
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.INFO)
        self.host = host
        self.port = port
        self.connection, self.connection_status = self.init_connection()
        self.dbname = db
        self.database = self.connection[db]  # link to database
        self.collection_neighbourhoods = self.database[coll_neighbourhoods]  # link to neighbourhoods collection
        self.collection_indicators = self.database[coll_indic]  # link to indicators collection (used for prediction)

    @staticmethod
    def bson_to_json(doc_bson):
        """
        Converts the bson data to valid JSON (including the ObjectId type converted to {"$oid": <string>}).

        Args:
            doc_bson: the BSON data to be converted

        Returns:
            doc_json: a JSON object
        """
        doc_json = json.loads(json_util.dumps(doc_bson, json_options=json_util.RELAXED_JSON_OPTIONS))
        return doc_json

    def init_connection(self):
        """
        Tries to connect to MongoDB (default on localhost:27017). The output connection object do not provide 
        reliable connection status, only the boolean connection_status indicates whether the connection is 
        working or not.

        Returns:
            connection: an object with information about the connection
            connection_status: a boolean indicating whether the connection is a success or a failure
        """
        connection_status = True  # by default, connection is ok
        connection = None
        self.logger.info(f'Trying to connect to MongoDB server ({self.host}:{self.port})...')
        try:
            connection = pymongo.MongoClient(self.host, self.port, serverSelectionTimeoutMS=max_timeout)
            connection.server_info()  # forces a query to MongoDB (for checking the connection)
        except pymongo.errors.ServerSelectionTimeoutError as e:
            self.logger.error('Could not connect to the MongoDB database ! Have you launched MongoDB ? ' + str(e))
            connection_status = False
        return connection, connection_status

    @staticmethod
    def _parse_json_to_dict(json_file_path):
        """ Converts a JSON file denoted by json_file_path into a Python dictionary. """
        with open(json_file_path) as data_file:
            data = json.load(data_file)
            data_file.close()
            return data

    @staticmethod
    def _save_dict_to_json(json_file_path, dict_geo):
        """ Converts and saves a Python dictionary into a JSON file denoted by json_file_path. """
        with open(json_file_path, 'w') as data_file:
            json.dump(dict_geo, data_file)

    def create_index(self, iris_collection):
        """ Rebuilds a geospatial index on the iris collection. Only used in case of restoration/import. """
        self.logger.info("Creating index on 'geometry' using " + pymongo.GEOSPHERE)
        iris_collection.create_index([("geometry", pymongo.GEOSPHERE)])
        self.logger.info("Index created")

    def count_documents(self, collection, json_query):
        """
        Counts the number of documents that satisfy json_query in the given collection.

        Args:
            collection: a string representing the collection name
            json_query: a dict containing the query criteria (https://docs.mongodb.com/manual/tutorial/query-documents/)

        Returns:
            count: an integer representing the number of documents
        """
        return collection.count_documents(json_query)

    def find_one_document(self, collection, json_query):
        """
        Finds the first document in the given collection that satisfies json_query.

        Args:
            collection: a string representing the collection name
            json_query: a dict containing the query criteria (https://docs.mongodb.com/manual/tutorial/query-documents/)

        Returns:
            doc_json: a dictionary representing an iris, or None
        """
        doc = collection.find_one(json_query)
        doc_json = Mongiris.bson_to_json(doc)
        return doc_json
        
    def find_all(self, collection):
        """
        Finds all documents in the given collection.
        
        Args:
            collection: a string representing the collection name
        
        Returns:
            doc_json: a dictionnary representing the documents, or None
        """
        doc = collection.find()
        doc_json = Mongiris.bson_to_json(doc)
        return doc_json

    def find_all(self, collection):
        """
        Finds all documents in the given collection.
        
        Args:
            collection: a string representing the collection name
        
        Returns:
            doc_json: a dictionnary representing the documents, or None
        """
        doc = collection.find()
        doc_json = Mongiris.bson_to_json(doc)
        return doc_json

    def get_neighbourhood_from_code(self, code_neighbourhood):
        """
        Returns the neighbourhood identified by the given code_neighbourhood.

        Args:
            code_neighbourhood: a string containing the code of the searched neighbourhood

        Returns:
            neighbourhood: a dictionary representing a neighbourhood, or None
        """
        neighbourhood = self.find_one_document(self.collection_neighbourhoods, {"properties.CODE_IRIS": code_neighbourhood})
        return neighbourhood

    def find_documents(self, collection, json_query, json_projection=None):
        """
        Finds the first document in the given collection that satisfies json_query.

        Args:
            collection: a string representing the collection name
            json_query: a dict containing the query criteria (https://docs.mongodb.com/manual/tutorial/query-documents/)
            json_projection: a json document indicating the fields that appear in the results

        Returns:
            doc_json: a cursor (set of documents)
        """
        cursor = collection.find(json_query, json_projection)
        doc_json = Mongiris.bson_to_json(cursor)
        return doc_json

    def get_random_document(self, collection):
        """ Returns a random document from the given collection. """
        random_iris = collection.aggregate([{"$sample": {"size": 1}}]).next()
        doc_json = Mongiris.bson_to_json(random_iris)
        return doc_json

    def update_one_document(self, collection, json_query, json_updates):
        """
        Updates the first document satisfying json_query by setting new values from json_updates.

        Args:
            collection: a string representing the collection name
            json_query: a dict containing the query criteria (https://docs.mongodb.com/manual/tutorial/query-documents/)
            json_updates: a json document containing values to be updates (using $set operator)

        Returns:
            json_result: an UpdateResult json document containing information about the operation
        """
        json_result = collection.update_one(json_query, json_updates)
        return json_result

    def replace_one_document(self, collection, json_query, json_replace_doc, upsert=False):
        """
        Replaces the first document satisfying json_query by the document json_replace_doc (their _id are identical).

        Args:
            collection: a string representing the collection name
            json_query: a dict containing the query criteria (https://docs.mongodb.com/manual/tutorial/query-documents/)
            json_replace_doc: the replacement doc (if _id set, should be the same _id as the doc matching json_query)
            upsert: a boolean, whether the json_replace_doc should be inserted if no document match json_query

        Returns:
            json_result: an UpdateResult json document containing information about the operation
        """
        json_result = collection.replace_one(json_query, json_replace_doc, upsert)
        return json_result

    def insert_one_document(self, collection, doc):
        """
        Inserts a new document in the collection.

        Args:
            collection: a string representing the collection name
            doc: a dict representing the document to be added

        Returns:
            json_result: an InsertOneResult json document containing information about the operation
        """
        json_result = collection.insert_one(doc)
        return json_result  # eg, the new _id is in json_result.inserted_id

    def delete_all(self, collection):
        """
        Deletes all documents in the collection.

        Args:
            collection: a string representing the collection name

        Returns:
            json_result: an DeleteResult json document containing information about the operation
        """
        json_result = collection.delete_many({})  # empty collection
        return json_result

    def geo_within(self, collection, geometry, json_projection=None):
        """
        Finds all documents from given collection and which contain totally the given geometry.
        Cannot be used to find the neighbourhood containing a point (geometry must be a polygon).

        Args:
            collection: a string representing the collection name
            geometry: a geojson geometry ("Polygon", "$box" or "MultiPolygon", NO "Point")
            json_projection: a json document indicating the fields that appear in the results

        Returns:
            doc_json: a cursor (set of documents)
        """
        cursor = self.find_documents(collection, {"geometry": {"$geoWithin": {"$geometry": geometry}}}, json_projection)
        doc_json = Mongiris.bson_to_json(cursor)
        return doc_json

    def geo_within_sphere(self, collection, sphere, json_projection=None):
        """
        Finds all documents from given collection and which contain totally the given sphere.
        Cannot be used to find the neighbourhood containing a point (geometry must be a polygon, with min. 3 points).

        Args:
            collection: a string representing the collection name
            sphere: a geojson geometry defined by a center and a radius in radians
            json_projection: a json document indicating the fields that appear in the results

        Returns:
            doc_json: a cursor (set of documents)
        """
        cursor = self.find_documents(collection, {"geometry": {"$geoWithin": sphere}}, json_projection)
        doc_json = Mongiris.bson_to_json(cursor)
        return doc_json

    def intersect(self, collection, geometry, json_projection=None):
        """
        Finds all documents from given collection and which intersect the given geometry.

        Args:
            collection: a string representing the collection name
            geometry: a geojson geometry
            json_projection: a json document indicating the fields that appear in the results

        Returns:
            doc_json: a cursor (set of documents)
        """
        cursor = self.find_documents(collection, {"geometry": {"$geoIntersects": {
            "$geometry": geometry}}}, json_projection)
        doc_json = Mongiris.bson_to_json(cursor)
        return doc_json

    @staticmethod
    def get_geojson_point(coordinates):
        """
        Builds a dictionary with GeoJSON syntax for a point using the given coordinates.

        Args:
            coordinates: the coordinates (long, lat) as a list, e.g. [4.8, 45.7]

        Returns:
            point: a dictionary with GeoJSON syntax for a Point
        """
        return {"type": "Point", "coordinates": coordinates}

    @staticmethod
    def convert_geojson_box_to_polygon(lng1, lat1, lng2, lat2):
        """
        Builds a dictionary with GeoJSON syntax for a polygon using two coordinates (points south-west and north-east).
        This method builds the polygon by adding 2 missing points (north-west and south-east) and adds the starting
        point (south-west) to end the loop.
        The MongoDB $box operator is not supported with 2d-spherical indexes.

        Args:
            lng1: longitude of the first point (south-west) of the box
            lat1: latitude of the first point (south-west) of the box
            lng2: longitude of the second point (north-east) of the box
            lat2: latitude of the second point (north-east) of the box

        Returns:
            box: a dictionary with GeoJSON syntax for a Box
        """
        coordinates = [[[lng1, lat1], [lng1, lat2], [lng2, lat2], [lng2, lat1], [lng1, lat1]]]
        return Mongiris.get_geojson_polygon(coordinates)

    @staticmethod
    def get_geojson_polygon(coordinates):
        """
        Builds a dictionary with GeoJSON syntax for a polygon using the given coordinates.
        Careful: polygons must be closed ! (first coordinate must be identical to last coordinate)

        Args:
            coordinates: the coordinates (long, lat) as a list of list, e.g. [[[4.8, 45.7], [4.9, 47.8]]]

        Returns:
            polygon: a dictionary with GeoJSON syntax for a Polygon
        """
        return {"type": "Polygon", "coordinates": coordinates}

    def point_in_which_neighbourhood(self, coordinates, json_projection=None):
        """
        Finds the document (neighbourhood) containing the given coordinates. Uses near() since geo_within() requires a
        Polygon.
        Careful: the near() operator may return several neighbourhoods (low probability since distance = 1 meter) and
        only the first one is returned.

        Args:
            coordinates: an array of coordinates (long, lat)
            json_projection: a json document indicating the fields that appear in the results

        Returns:
            doc_json: a json document or None
        """
        results = self.near(self.collection_neighbourhoods, coordinates, json_projection, 1)  # distance = 1 meter
        if len(results) == 0:
            return None
        return results[0]

    def near(self, collection, coordinates, json_projection=None, distance_max=2000):
        """
        Finds all documents from given collection and which are near the given geometry (according to distance_max).

        Args:
            collection: a string representing the collection name
            coordinates: an array of coordinates (long, lat) - near only accepts Point
            json_projection: a json document indicating the fields that appear in the results
            distance_max: the maximum distance of resulting iris, in meters

        Returns:
            doc_json: a cursor (set of documents)
        """
        geometry = Mongiris.get_geojson_point(coordinates)
        cursor = self.find_documents(collection, {"geometry": {"$near": {
            "$geometry": geometry, "$maxDistance": distance_max}}}, json_projection)
        doc_json = Mongiris.bson_to_json(cursor)
        return doc_json

    @staticmethod
    def adjacent(collection, geometry, json_projection=None, distance=20, exclude_geometry=None):
        """
        Finds all adjacent neighbors of a neighbourhood represented by geometry.
        No adjacent function, so use all coordinates of an iris and find the closest neighbourhood (according to
        distance). Could be done directly with near(), but near() is less accurate and thus incomplete.

        Args:
            collection: a string representing the collection name
            geometry: a geojson geometry (Point, Polygon, etc.)
            json_projection: a json document indicating the fields that appear in the results
            distance: the maximum distance for an adjacent neighbour, in meters (10 to 50 meters are fine)
            exclude_geometry: the document _id of the neighbourhood represented by geometry, if it needs to be excluded

        Returns:
            doc_json: a cursor (set of documents)
        """
        results = list()
        results_ids = list()
        if exclude_geometry is not None:  # to exclude the neighbourhood represented by geometry, add it to the results ids
            results_ids.append(exclude_geometry)
        for coords in geometry["coordinates"][0]:
            geometry_coords = Mongiris.get_geojson_point(coords)
            cursor = collection.find({"geometry": {"$near": {"$geometry": geometry_coords,
                                                             "$maxDistance": distance}}}, json_projection)
            for doc in cursor:
                doc_id = doc["_id"]
                if doc_id not in results_ids:  # add the new adjacent iris if not already in the results
                    results.append(doc)
                    results_ids.append(doc_id)
        doc_json = Mongiris.bson_to_json(results)
        return doc_json


if __name__ == "__main__":
    print("Run unit tests for testing the Mongiris class.")
