#!/usr/bin/env python
# encoding: utf-8
# =============================================================================
# Unit tests for mongiris.
# =============================================================================

# eternal hack for python imports (here to access parent directory)
import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from api import Mongiris
import unittest
import random
import re

host = "localhost"
port = 27017

class TestCase(unittest.TestCase):
    """
    A class for Mongiris unit tests.
    Some tests select a random neighbourhood in the collection,so there is no assert check.
    """

    def setUp(self):
        # a setup (connection to MongoDB) executed before each test
        self.db = Mongiris(host=host, port=port, db='dbinsee', coll_neighbourhoods='colliris')

    def test_count(self):
        # test for counting the number of documents
        count = self.db.count_documents(self.db.collection_neighbourhoods, {})
        assert (count == 49404), f'Error: expecting 49404 neighbourhoods stored in database dbinsee, but found {count}'

    def test_find_one(self):
        # test for finding a specific document using a field CODE_IRIS
        neighbourhood = self.db.find_one_document(self.db.collection_neighbourhoods, {"properties.CODE_IRIS": "593500203"})
        assert(neighbourhood is not None), 'Function get_iris_from_code("593500203") should return one document, not None'
        self.db.logger.info(neighbourhood)

    def test_find_documents(self):
        # test for finding documents containing a given string (either in teh city name or in the neighbourhood name)
        regx = re.compile("Lyon", re.IGNORECASE)
        query_clause = {"$or": [{"properties.NOM_IRIS": {"$regex": regx}}, {"properties.NOM_COM": {"$regex": regx}}]}
        cursor = self.db.find_documents(self.db.collection_neighbourhoods, query_clause)
        for doc in cursor:
            self.db.logger.info(str(doc["_id"]) + "\t" + doc["properties"]["NOM_IRIS"] + "\t" + doc["properties"]["CODE_IRIS"] + "\t" + doc["properties"]["NOM_COM"])
        assert (len(cursor) == 203), "Method test_find_documents() for Lyon should find 203 neighbourhoods, not " + str(len(cursor))

    def test_get_neighbourhood_from_code(self):
        # test for retrieving an neighbourhood from code
        neighbourhood = self.db.get_neighbourhood_from_code("593500203")
        assert(neighbourhood is not None), 'Function find_one_document(..., "593500203") should return one document, not None'
        self.db.logger.info(neighbourhood)

    def test_op_geo_within(self):
        # test for a geospatial query that returns all neighbourhood within a given polygon
        self.db.logger.info("Finding all neighbourhood in area [[2.3530807599035124, 50.865983520113346], [2.607984899697411, 50.98885556985259]]")
        long1 = 2.3530807599035124
        lat1 = 50.865983520113346
        long2 = 2.607984899697411
        lat2 = 50.98885556985259
        polygon = Mongiris.convert_geojson_box_to_polygon(long1, lat1, long2, lat2)
        cursor = self.db.geo_within(self.db.collection_neighbourhoods, polygon)
        for doc in cursor:
            self.db.logger.info(str(doc["_id"]) + "\t" + doc["properties"]["NOM_IRIS"] + "\t" + doc["properties"]["CODE_IRIS"])
        assert(len(cursor) == 12), "Method test_op_geo_within() should find 12 neighbourhoods, not " + str(len(cursor))

    def test_op_near(self):
        # test for a geospatial query that returns all neighbourhood within a distance of a given neighbourhood
        random_iris = self.db.get_random_document(self.db.collection_neighbourhoods)
        point_random_iris = random.choice(random_iris["geometry"]["coordinates"][0])  # get a random coordinate
        self.db.logger.info("Finding all near neighbourhoods for " + random_iris["properties"]["NOM_IRIS"] + " " +
                            random_iris["properties"]["CODE_IRIS"])
        distance_max = 3000  # in meters
        cursor = self.db.near(self.db.collection_neighbourhoods, point_random_iris, {"properties.NOM_IRIS": 1, "properties.CODE_IRIS": 1},
                              distance_max)
        for doc in cursor:
            self.db.logger.info(str(doc["_id"]) + "\t" + doc["properties"]["NOM_IRIS"] + "\t" + doc["properties"]["CODE_IRIS"])

    def test_op_intersect(self):
        # test for a geospatial query that returns all neighbourhood that intersect a given neighbourhood
        random_iris = self.db.get_random_document(self.db.collection_neighbourhoods)
        geometry_random_iris = random_iris["geometry"]
        self.db.logger.info("Finding all intersecting neighbourhoods for " + random_iris["properties"]["NOM_IRIS"] + " " +
                            random_iris["properties"]["CODE_IRIS"])
        cursor = self.db.intersect(self.db.collection_neighbourhoods, geometry_random_iris, {"properties.NOM_IRIS": 1, "properties.CODE_IRIS": 1})
        for doc in cursor:
            self.db.logger.info(str(doc["_id"]) + "\t" + doc["properties"]["NOM_IRIS"] + "\t" + doc["properties"]["CODE_IRIS"])

    def test_op_adjacent(self):
        # test for a geospatial query that returns all neighbourhood adjacent to a given neighbourhood (or very close, < 10 meters)
        random_iris = self.db.get_random_document(self.db.collection_neighbourhoods)
        geometry_random_iris = random_iris["geometry"]
        self.db.logger.info("Finding all adjacent neighbourhoods for " + random_iris["properties"]["NOM_IRIS"] + " " +
                            random_iris["properties"]["CODE_IRIS"])
        distance_max = 10  # in meters
        results = self.db.adjacent(self.db.collection_neighbourhoods, geometry_random_iris, {"properties.NOM_IRIS": 1, "properties.CODE_IRIS": 1},
                                   distance_max, random_iris["_id"])
        for doc in results:
            self.db.logger.info(str(doc["_id"]) + "\t" + doc["properties"]["NOM_IRIS"] + "\t" + doc["properties"]["CODE_IRIS"])

    def test_op_point_in_which_neighbourhood(self):
        # tests for a geospatial query that returns the neighbourhood (or None) which contains a given point
        # test 1
        self.db.logger.info("Finding neighbourhood for (3.685111, 46.514643)")
        doc = self.db.point_in_which_neighbourhood([3.685111, 46.514643])
        assert (doc is not None), 'Coordinates (3.685111, 46.514643) should return one document, not None'
        assert (doc["properties"]["CODE_IRIS"] == '031020000'), 'Coordinates (3.685111, 46.514643) should correspond to neighbourhood 031020000'
        self.db.logger.info(str(doc["_id"]) + "\t" + doc["properties"]["NOM_IRIS"] + "\t" + doc["properties"]["CODE_IRIS"])
        # test 2
        self.db.logger.info("Finding neighbourhood for (5.685111, 46.514643)")
        doc = self.db.point_in_which_neighbourhood([5.685111, 46.514643])
        assert (doc is not None), 'Coordinates (5.685111, 46.514643) should return one document, not None'
        assert (doc["properties"]["CODE_IRIS"] == '391750000'), 'Coordinates (5.685111, 46.514643) should correspond to neighbourhood 391750000'
        self.db.logger.info(str(doc["_id"]) + "\t" + doc["properties"]["NOM_IRIS"] + "\t" + doc["properties"]["CODE_IRIS"])
        # test 3
        self.db.logger.info("Finding neighbourhood for (-49.016200, 79.333879)")
        doc = self.db.point_in_which_neighbourhood([-49.016200, 79.333879])
        assert (doc is None), 'Coordinates (79.333879, -49.016200) should return no document (no corresponding neighbourhood)'

    def test_switch_db(self):
        self.db = Mongiris(host=host, port=port, db='dbmigration', coll_neighbourhoods='collmigration')
        self.db.logger.info("Connection to another database (dbmigration).")
        count = self.db.count_documents(self.db.collection_neighbourhoods, {})
        assert (count == 769), f'Error: expecting 769 neighbourhoods stored in datase dbmigration, but found {count}'

    def test_test(self):
        # for testing code
        coord = [4.8300768, 45.720019]
        cursor = self.db.near(self.db.collection_neighbourhoods, coord, {"properties.NOM_IRIS": 1,
                                                                         "properties.CODE_IRIS": 1}, 1000)
        for doc in cursor:
            self.db.logger.info(str(doc["_id"]) + "\t" + doc["properties"]["NOM_IRIS"] + "\t" +
                                doc["properties"]["CODE_IRIS"])


if __name__ == "__main__":
    unittest.main(verbosity=2)  # run all tests with verbose mode

