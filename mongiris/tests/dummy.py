#!/usr/bin/env python
# encoding: utf-8
# =============================================================================
# Dummy test for mongiris.
# =============================================================================


from mongiris.api import Mongiris
import json


db = Mongiris()

# return the number of documents in a collection
counts = db.count_documents(db.collection_indicators, {})

# get complete information about iris identified with code 593500203
iris = db.find_one_document(db.collection_neighbourhoods, {"properties.CODE_IRIS": "593500203"})

# get iris which contains coordinates 3.685111, 46.514643
iris2 = db.point_in_which_neighbourhood([3.685111, 46.514643])

print(counts)
print(iris)
print(iris2)

#print(json.dumps(iris, indent=4, sort_keys=True))
